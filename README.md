# react-currency-input

> react currency input 
> Work in progress

[![NPM](https://img.shields.io/npm/v/react-currency-input.svg)](https://www.npmjs.com/package/react-currency-input) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save react-currency-input
```

## Usage

```tsx
import React, { Component } from 'react'

import ReactCurrencyInput from 'react-currency-input'
import 'react-currency-input/dist/index.css' // or use class react-currency-input for your own styles

class Example extends Component {
  render() {
    return <ReactCurrencyInput />
  }
}
```

## License

MIT © [AhmedCommando](https://github.com/AhmedCommando)
